import csv
import pandas as pd
import scrapy

# YIELD x 8 -> PARSE x 8
class JSummarySpider(scrapy.Spider):
    name = "job_summary"
    raw_input = ""
    g_summary = []
    links = {}
    job_summaries = {}
    summ_iterator = 0
    b_key = 0
    parse_iterator = 0

    def start_requests(self):
        i = 0
        a_key = 0
        with open('input\jobslist.csv', 'rb') as csvfile:
            next(csvfile)  # disregard header row
            filereader = csv.reader(csvfile)
            for row in filereader:
                for item in row:
                    item = item + '\n'
                    self.raw_input += item

            my_list = self.raw_input.splitlines()
            for item in my_list:
                if i == 3 or i%11 == 3:
                    #yield scrapy.Request(url = item, callback = self.parse)
                    self.links[a_key] = item
                    a_key += 1
                i += 1
            for key in self.links:
                yield scrapy.Request(url = self.links[key], callback = self.parse)

    def parse(self,response):
        dummy = {'asdasd': "AAAAAAAAAW", 'as2': "Summary"}
        print "yield!" + str(self.parse_iterator)
        summary = (response.selector.xpath("//span[@id='job_summary']//text()").extract())
        string1 = ''.join(summary)
        self.parse_iterator += 1

        csv_input = pd.read_csv('input\jobslist.csv')
        for key in dummy:
            csv_input['Summary'] = dummy[key]
            print(key, dummy[key])
        csv_input.to_csv('output.csv', index=False)